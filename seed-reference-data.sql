-- Database: db-sandbox1

-- DROP DATABASE IF EXISTS "db-sandbox1";

INSERT INTO PUBLIC.TASK_DEFINITION(NAME, TYPE)
VALUES ('matching-pairs', 'memory-game');

INSERT INTO PUBLIC.TASK_DEFINITION(NAME, TYPE)
VALUES ('video-cartoon', 'watching-video');