truncate person restart identity cascade;
truncate lab_session restart identity cascade;

select * from lab_session;
select * from participant_image;
select * from person;
select * from task_attempt;
select * from task_definition;
select * from task_interaction;
select * from task_performance;