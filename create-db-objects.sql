-- Database: db-sandbox1

-- DROP DATABASE IF EXISTS "db-sandbox1";

CREATE DATABASE "db-sandbox1"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_Australia.1252'
    LC_CTYPE = 'English_Australia.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


-- SCHEMA: public

-- DROP SCHEMA IF EXISTS public ;

CREATE SCHEMA IF NOT EXISTS public
    AUTHORIZATION postgres;

COMMENT ON SCHEMA public
    IS 'standard public schema';

GRANT ALL ON SCHEMA public TO PUBLIC;

GRANT ALL ON SCHEMA public TO postgres;


-- Table: public.task_definition

-- DROP TABLE IF EXISTS public.task_definition;

CREATE TABLE IF NOT EXISTS public.task_definition
(
    name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    type character varying(100) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT task_definition_pkey PRIMARY KEY (name)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.task_definition
    OWNER to postgres;


-- Table: public.person

-- DROP TABLE IF EXISTS public.person;

CREATE TABLE IF NOT EXISTS public.person
(
    id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    username character varying(100) COLLATE pg_catalog."default" NOT NULL,
    gender character varying(10) COLLATE pg_catalog."default",
    race character varying(20) COLLATE pg_catalog."default",
    year_of_birth smallint,
    CONSTRAINT person_pkey PRIMARY KEY (id),
    CONSTRAINT person_username_key UNIQUE (username)
        INCLUDE(username)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.person
    OWNER to postgres;


-- Table: public.lab_session

-- DROP TABLE IF EXISTS public.lab_session;

CREATE TABLE IF NOT EXISTS public.lab_session
(
    id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    person_id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    webcam_capture_consent boolean NOT NULL,
    start_ts bigint NOT NULL,
    end_ts bigint,
    video_file_id character varying(32) COLLATE pg_catalog."default",
    video_start_ts bigint,
    video_end_ts bigint,
    CONSTRAINT lab_session_pkey PRIMARY KEY (id),
    CONSTRAINT lab_session_person_id_fkey FOREIGN KEY (person_id)
        REFERENCES public.person (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.lab_session
    OWNER to postgres;

COMMENT ON COLUMN public.lab_session.video_file_id
    IS 'Identifier for the video file.';

COMMENT ON COLUMN public.lab_session.video_start_ts
    IS 'When the video identified by video_file_id started as timestamp in millis.';

COMMENT ON COLUMN public.lab_session.video_end_ts
    IS 'When the video identified by video_file_id ended as timestamp in millis.';


-- Table: public.task_attempt

-- DROP TABLE IF EXISTS public.task_attempt;

CREATE TABLE IF NOT EXISTS public.task_attempt
(
    id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    session_id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    task_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    start_ts bigint NOT NULL,
    end_ts bigint,
    CONSTRAINT task_attempt_pkey PRIMARY KEY (id),
    CONSTRAINT task_attempt_session_id_fkey FOREIGN KEY (session_id)
        REFERENCES public.lab_session (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT task_attempt_task_name_fkey FOREIGN KEY (task_name)
        REFERENCES public.task_definition (name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.task_attempt
    OWNER to postgres;


-- Table: public.task_interaction

-- DROP TABLE IF EXISTS public.task_interaction;

CREATE TABLE IF NOT EXISTS public.task_interaction
(
    id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    task_attempt_id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    type character varying(100) COLLATE pg_catalog."default" NOT NULL,
    outcome character varying(100) COLLATE pg_catalog."default",
    start_ts bigint NOT NULL,
    end_ts bigint,
    CONSTRAINT task_interaction_pkey PRIMARY KEY (id),
    CONSTRAINT task_interaction_task_attemp_id_fkey FOREIGN KEY (task_attempt_id)
        REFERENCES public.task_attempt (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.task_interaction
    OWNER to postgres;

COMMENT ON TABLE public.task_interaction
    IS 'Contains timestamped interactions of a participant during a task.';

COMMENT ON COLUMN public.task_interaction.type
    IS 'Type of interaction of the participant. E.g. action and non-action clicks and keyboard key presses, reading task description, memorising, recalling, reading question, thinking, submitting answer, etc.';

COMMENT ON COLUMN public.task_interaction.outcome
    IS 'Outcome of the interaction where applicable. For instance, if submitted answer, was it correct/incorrect, if made a shot, did it hit the target.';


-- Table: public.task_performance

-- DROP TABLE IF EXISTS public.task_performance;

CREATE TABLE IF NOT EXISTS public.task_performance
(
    id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    task_attempt_id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    completed boolean,
    duration integer,
    outcome character varying(10) COLLATE pg_catalog."default",
    score smallint,
    CONSTRAINT task_performance_pkey PRIMARY KEY (id),
    CONSTRAINT task_performance_task_atttempt_id_fkey FOREIGN KEY (task_attempt_id)
        REFERENCES public.task_attempt (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.task_performance
    OWNER to postgres;

COMMENT ON COLUMN public.task_performance.duration
    IS 'Amount of taken in milliseconds to complete the task.';

COMMENT ON COLUMN public.task_performance.outcome
    IS 'If this is a game task and the participant can win/lose, this will indicate whether they have lost or won.';

COMMENT ON COLUMN public.task_performance.score
    IS 'For tasks where the participant gainse/loses points, this field is the net score as calculated by the task itself. E.g. in a matching pairs game, each tile reveal beyond the minimum tile reveals required to find all matches will yield a -ve point.';


-- Table: public.participant_image

-- DROP TABLE IF EXISTS public.participant_image;

CREATE TABLE IF NOT EXISTS public.participant_image
(
    id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    shot bytea,
    "timestamp" bigint NOT NULL,
    session_id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    face bytea,
    right_eye bytea,
    left_eye bytea,
    right_eye_tensor_state character varying(10) COLLATE pg_catalog."default",
    right_eye_tensor_state_probability numeric(5,4),
    left_eye_tensor_state character varying(10) COLLATE pg_catalog."default",
    left_eye_tensor_state_probability numeric(5,4),
    frame_number integer NOT NULL,
    blinking boolean,
    CONSTRAINT participant_image_pkey PRIMARY KEY (id),
    CONSTRAINT participant_image_session_id_fkey FOREIGN KEY (session_id)
        REFERENCES public.lab_session (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.participant_image
    OWNER to postgres;

COMMENT ON COLUMN public.participant_image.shot
    IS 'The entire shot captured';

COMMENT ON COLUMN public.participant_image.face
    IS 'Face image extracted from frame';

COMMENT ON COLUMN public.participant_image.right_eye
    IS 'Image of the right eye extracted from the frame';

COMMENT ON COLUMN public.participant_image.left_eye
    IS 'Image of the left eye extracted from the image frane';

COMMENT ON COLUMN public.participant_image.right_eye_tensor_state
    IS 'State of three consecutive eye images stacked on each other to form an ''RGB'' image.';

COMMENT ON COLUMN public.participant_image.right_eye_tensor_state_probability
    IS 'The probability of the predicted state for the right eye tensor as a fraction between 0 and 1 inclusive.';

COMMENT ON COLUMN public.participant_image.left_eye_tensor_state
    IS 'State of three consecutive left eye images stacked on each other to form an ''RGB'' image.';

COMMENT ON COLUMN public.participant_image.left_eye_tensor_state_probability
    IS 'The probability of the predicted state for the left eye tensor as a fraction between 0 and 1 inclusive.';

COMMENT ON COLUMN public.participant_image.frame_number
    IS 'The number of the frame of the stream of images captured.';

COMMENT ON COLUMN public.participant_image.blinking
    IS 'A boolean specifying whether this frame is part of a blink.';


-- Table: public.blink_analysis

-- DROP TABLE IF EXISTS public.blink_analysis;

CREATE TABLE IF NOT EXISTS public.blink_analysis
(
    id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    session_id character varying(32) COLLATE pg_catalog."default" NOT NULL,
    sequence smallint NOT NULL,
    start_ts bigint NOT NULL,
    start_frame integer NOT NULL,
    end_ts bigint NOT NULL,
    end_frame integer NOT NULL,
    duration smallint NOT NULL,
    "interval" smallint,
    CONSTRAINT blink_analysis_pkey PRIMARY KEY (id),
    CONSTRAINT blink_analysis_session_id_fkey FOREIGN KEY (session_id)
        REFERENCES public.lab_session (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.blink_analysis
    OWNER to postgres;

COMMENT ON COLUMN public.blink_analysis.sequence
    IS 'Sequence number of this blink in the series of blinks for this session.';

COMMENT ON COLUMN public.blink_analysis.start_ts
    IS 'Timestamp in millis when the blink started.';

COMMENT ON COLUMN public.blink_analysis.start_frame
    IS 'Frame number at which this blink started.';

COMMENT ON COLUMN public.blink_analysis.end_ts
    IS 'Timestamp in millis when the blink ended.';

COMMENT ON COLUMN public.blink_analysis.end_frame
    IS 'Frame number at which this blink ended.';

COMMENT ON COLUMN public.blink_analysis.duration
    IS 'Duration of this blink in millis.';

COMMENT ON COLUMN public.blink_analysis."interval"
    IS 'Time in millis since the last blink. If this is the first blink, then this will be blank.';
